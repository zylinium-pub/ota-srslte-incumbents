#!/usr/bin/python

"""This profile allows the allocation of resources for over-the-air
operation on the POWDER platform. Specifically, the profile has
options to request the allocation of SDR radios in rooftop
base-stations and fixed-endpoints (i.e., nodes deployed at
human height).

Map of deployment is here:
https://www.powderwireless.net/map

The base-station SDRs are X310s and connected to an antenna
covering the cellular band (1695 - 2690 MHz), i.e., cellsdr,
or to an antenna covering the CBRS band (3400 - 3800 MHz), i.e.,
cbrssdr. Each X310 is paired with a compute node (by default
a Dell d740).

The fixed-endpoint SDRs are B210s each of which is paired with
an Intel NUC small form factor compute node. Both B210s are connected
to broadband antennas: nuc1 is connected in an RX only configuration,
while nuc2 is connected in a TX/RX configuration.

The profile uses a disk image with srsLTE software, as well as
GNU Radio and the UHD software tools, pre-installed.

Resources needed to realize a basic srsLTE setup consisting of a UE, an eNodeB and an EPC core network:

  * Frequency ranges (uplink and downlink) for LTE FDD operation.
  * A "nuc2" fixed-end point compute/SDR pair. (This will run the UE side.)
  * A "cellsdr" base station SDR. (This will be the radio side of the eNodeB.)
  * A "d740" compute node. (This will run both the eNodeB software and the EPC software.)

**Specific resources that can be used (and that need to be reserved before instantiating the profile):**

  * Hardware (at least one set of resources are needed):
   * Humanities, nuc2; Emulab, cellsdr1-browning; Emulab, d740
   * Bookstore, nuc2; Emulab, cellsdr1-bes; Emulab, d740
   * Moran, nuc2; Emulab, cellsdr1-ustar; Emulab, d740
  * Frequencies:
   * Uplink frequency: 2500 MHz to 2510 MHz
   * Downlink frequency: 2530 MHz to 2540 MHz

The instuctions below assume the first hardware configuration.

Instructions:

The instructions below assume the following hardware set was selected when the profile was instantiated:

 * Bookstore, nuc2; Emulab, cellsdr1-browning; Emulab, d740

#### To run the srsLTE software

**To run the EPC**

Open a terminal on the `cellsdr1-browning-comp` node in your experiment. (Go to the "List View"
in your experiment. If you have ssh keys and an ssh client working in your
setup you should be able to click on the black "ssh -p ..." command to get a
terminal. If ssh is not working in your setup, you can open a browser shell
by clicking on the Actions icon corresponding to the node and selecting Shell
from the dropdown menu.)

Start up the EPC:

    sudo srsepc

**To run the eNodeB**

Open another terminal on the `cellsdr1-browning-comp` node in your experiment.

Start up the eNodeB:

    sudo srsenb

**To run the UE**

Open a terminal on the `b210-bookstore-nuc2` node in your experiment.

Start up the UE:

    sudo srsue

**Verify functionality**

Open another terminal on the `b210-bookstore-nuc2` node in your experiment.

Verify that the virtual network interface tun_srsue" has been created:

    ifconfig tun_srsue

Run ping to the SGi IP address via your RF link:

    ping 172.16.0.1

Killing/restarting the UE process will result in connectivity being interrupted/restored.

If you are using an ssh client with X11 set up, you can run the UE with the GUI
enabled to see a real time view of the signals received by the UE:

    sudo srsue --gui.enable 1

Note: If srsenb fails with an error indicating "No compatible RF-frontend
found", you'll need to flash the appropriate firmware to the X310 and
power-cycle it using the portal UI. Run `uhd_usrp_probe` in a shell on the
associated compute node to get instructions for downloading and flashing the
firmware. Use the Action buttons in the List View tab of the UI to power cycle
the appropriate X310. If srsue fails with a similar error, try power-cycling the
associated NUC.

"""

import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig
import geni.rspec.emulab.spectrum as spectrum


class GLOBALS:
    #SRSLTE_IMG = "urn:publicid:IDN+emulab.net+image+FiveGExperiments:zylinium-ota-srslte"
    SRSLTE_IMG = "urn:publicid:IDN+emulab.net+image+FiveGExperiments//zy-srs-incumbents"
    SRSLTE_SRC_DS = "urn:publicid:IDN+emulab.net:powderteam+imdataset+srslte-src-v19"
    DLHIFREQ = 2630.0
    DLLOFREQ = 2620.0
    ULHIFREQ = 2510.0
    ULLOFREQ = 2500.0

def validate_ip(s):
    a = s.split('.')
    if len(a) != 4:
        return False
    if int(a[0]) != 172:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True

def x310_node_pair(idx, x310_radio, collab_ip):
    radio_link = request.Link("radio-link-%d"%(idx))
    radio_link.bandwidth = 10*1000*1000

    node = request.RawPC("%s-comp"%(x310_radio.radio_name))
    node.hardware_type = params.x310_pair_nodetype
    node.disk_image = GLOBALS.SRSLTE_IMG
    node.component_manager_id = "urn:publicid:IDN+emulab.net+authority+cm"
    node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/add-nat-and-ip-forwarding.sh"))
    node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
    node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
    node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-sdr-iface.sh"))
    node.addService(rspec.Execute(shell="bash", command="sudo /usr/local/bin/radio.sh"))
    node.addService(rspec.Execute(shell="bash", command="sudo ip route add 155.98.47.0/24 via 155.98.36.204"))

    logs_bs = node.Blockstore("bs-logs-%d"%(idx), "/logs")
    logs_bs.size = "30GB"

    node_radio_if = node.addInterface("usrp_if")
    node_radio_if.addAddress(rspec.IPv4Address("192.168.40.1",
                                               "255.255.255.0"))
    radio_link.addInterface(node_radio_if)

    radio = request.RawPC("%s-x310"%(x310_radio.radio_name))
    radio.component_id = "%s"%(x310_radio.radio_name)
    radio_link.addNode(radio)

    if validate_ip(collab_ip):
        collab_if = node.addInterface("collab_if")
        collab_if_ip = collab_ip.split('.')[:-1]
        collab_if_ip.append(str(int(collab_ip.split('.')[-1]) + idx + 1))
        collab_if.addAddress(rspec.IPv4Address(".".join(collab_if_ip),
                                               "255.255.255.0"))
        return collab_if


def b210_nuc_pair(idx, b210_node):
    b210_nuc_pair_node = request.RawPC("%s"%(b210_node.radio_name))
    agg_full_name = "urn:publicid:IDN+%s.powderwireless.net+authority+cm"%(b210_node.radio_name.split('-')[0])
    b210_nuc_pair_node.component_manager_id = agg_full_name
    b210_nuc_pair_node.component_id = b210_node.radio_name.split('-')[-1]
    b210_nuc_pair_node.disk_image = GLOBALS.SRSLTE_IMG
    b210_nuc_pair_node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/update-config-files.sh"))
    b210_nuc_pair_node.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
    b210_nuc_pair_node.addService(rspec.Execute(shell="bash", command="sudo /usr/local/bin/radio.sh"))


    logs_bs = b210_nuc_pair_node.Blockstore("bs-logs-%d"%(idx), "/logs")
    logs_bs.size = "30GB"


node_type = [
    ("d740",
     "Emulab, d740"),
    ("d430",
     "Emulab, d430")
]

portal.context.defineParameter("x310_pair_nodetype",
                               "Type of compute node paired with the X310 Radios",
                               portal.ParameterType.STRING,
                               node_type[0],
                               node_type)

rooftop_names = [
    ("cellsdr1-bes",
     "Emulab, bes cell (Behavioral)"),
    ("cbrssdr1-bes",
     "Emulab, bes cbrs (Behavioral)"),
    ("cellsdr1-browning",
     "Emulab, browning cell (Browning)"),
    ("cbrssdr1-browning",
     "Emulab, browning cbrs (Browning)"),
    ("cellsdr1-dentistry",
     "Emulab, dentistry cell (Dentistry)"),
    ("cbrssdr1-dentistry",
     "Emulab, dentistry cbrs (Dentistry)"),
    ("cellsdr1-fm",
     "Emulab, fm cell (Friendship Manor)"),
    ("cbrssdr1-fm",
     "Emulab, fm cbrs (Friendship Manor)"),
    ("cellsdr1-honors",
     "Emulab, honors cell (Honors)"),
    ("cbrssdr1-honors",
     "Emulab, honors cbrs (Honors)"),
    ("cellsdr1-hospital",
     "Emulab, hospital cell (Hospital)"),
    ("cbrssdr1-hospital",
     "Emulab, hospital cbrs (Hospital)"),
    ("cellsdr1-meb",
     "Emulab, meb cell (MEB)"),
    ("cbrssdr1-meb",
     "Emulab, meb cbrs (MEB)"),
    ("cellsdr1-smt",
     "Emulab, smt cell (SMT)"),
    ("cbrssdr1-smt",
     "Emulab, smt cbrs (SMT)"),
    ("cellsdr1-ustar",
     "Emulab, ustar cell (USTAR)"),
    ("cbrssdr1-ustar",
     "Emulab, ustar cbrs (USTAR)")
]

portal.context.defineStructParameter("x310_nodes", "X310 Radios", [],
                                     multiValue=True,
                                     itemDefaultValue=
                                     {},
                                     min=0, max=None,
                                     members=[
                                        portal.Parameter(
                                             "radio_name",
                                             "Rooftop base-station X310",
                                             portal.ParameterType.STRING,
                                             rooftop_names[0],
                                             rooftop_names)
                                     ])

fixed_endpoint_aggregates = [
    ("web-nuc1",
     "WEB (NUC1)"),
    ("web-nuc2",
     "WEB (NUC2)"),
    ("ebc-nuc1",
     "EBC (NUC1)"),
    ("ebc-nuc2",
     "EBC (NUC2)"),
    ("bookstore-nuc1",
     "Bookstore (NUC 1)"),
    ("bookstore-nuc2",
     "Bookstore (NUC 2)"),
    ("humanities-nuc1",
     "Humanities (NUC 1)"),
    ("humanities-nuc2",
     "Humanities (NUC 2)"),
    ("law73-nuc1",
     "Law 73 (NUC 1)"),
    ("law73-nuc2",
     "Law 73 (NUC 2)"),
    ("madsen-nuc1",
     "Madsen (NUC 1)"),
    ("madsen-nuc2",
     "Madsen (NUC 2)"),
    ("sagepoint-nuc1",
     "Sage Point (NUC 1)"),
    ("sagepoint-nuc2",
     "Sage Point (NUC 2)"),
    ("moran-nuc1",
     "Moran (NUC 1)"),
    ("moran-nuc2",
     "Moran (NUC 2)"),
    ("guesthouse-nuc1",
     "Guest House (NUC 1)"),
    ("guesthouse-nuc2",
     "Guest House (NUC 2)"),
]

portal.context.defineStructParameter("b210_nodes", "B210 Radios", [],
                                     multiValue=True,
                                     min=0, max=None,
                                     members=[
                                         portal.Parameter(
                                             "radio_name",
                                             "Fixed Endpoint B210",
                                             portal.ParameterType.STRING,
                                             fixed_endpoint_aggregates[1],
                                             fixed_endpoint_aggregates)
                                     ],
                                    )

portal.context.defineParameter("ul_lofreq", "UL Low Frequency", portal.ParameterType.BANDWIDTH, GLOBALS.ULLOFREQ)
portal.context.defineParameter("ul_hifreq", "UL High Frequency", portal.ParameterType.BANDWIDTH, GLOBALS.ULHIFREQ)
portal.context.defineParameter("dl_lofreq", "DL Low Frequency", portal.ParameterType.BANDWIDTH, GLOBALS.DLLOFREQ)
portal.context.defineParameter("dl_hifreq", "DL High Frequency", portal.ParameterType.BANDWIDTH, GLOBALS.DLHIFREQ)
portal.context.defineParameter("collab_ip", "Collaboration Server IP Address", portal.ParameterType.STRING, "")

params = portal.context.bindParameters()
request = portal.context.makeRequestRSpec()

if params.ul_lofreq > 0 and params.ul_hifreq > 0:
    request.requestSpectrum(params.ul_lofreq, params.ul_hifreq, 0)

if params.dl_lofreq > 0 and params.dl_hifreq > 0:
    request.requestSpectrum(params.dl_lofreq, params.dl_hifreq, 0)


collab_link = request.LAN("collab")

for i, x310_radio in enumerate(params.x310_nodes):
    collab_if = x310_node_pair(i, x310_radio, params.collab_ip)
    if collab_if is not None:
        collab_link.addInterface(collab_if)

# Create collaboration server if IP specified
if validate_ip(params.collab_ip):
    node = request.RawPC("collab-server")
    node.hardware_type = params.x310_pair_nodetype
    node.disk_image = GLOBALS.SRSLTE_IMG
    node.component_manager_id = "urn:publicid:IDN+emulab.net+authority+cm"
    collab_if = node.addInterface("collab_if")
    collab_if.addAddress(rspec.IPv4Address(params.collab_ip,
                                           "255.255.255.0"))
    collab_link.addInterface(collab_if)
    node.addService(rspec.Execute(shell="bash", command="sudo /local/collaboration-protocol/python/collab_server.py --server-ip {} --log-config-filename /local/collaboration-protocol/python/collab_server_logging.conf".format(params.collab_ip)))

for i, b210_node in enumerate(params.b210_nodes):
    b210_nuc_pair(i + len(params.x310_nodes), b210_node)


portal.context.printRequestRSpec()
